package junia.lab03.web.controller;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import junia.lab03.domain.data.Project;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

@WebServlet(urlPatterns = "/api/*")
public class ApiServlet extends HttpServlet {

    private ProjectService projectService;
    private CompanyService companyService;
    private BusinessTypeService businessTypeService;
    private CustomerService customerService;

    private Map<String, Consumer<PrintWriter>> routes;


    @Override
    public void init() {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("junia.lab03.core.config");
        companyService = context.getBean(CompanyService.class);
        businessTypeService = context.getBean(BusinessTypeService.class);
        customerService = context.getBean(CustomerService.class);
        projectService = context.getBean(ProjectService.class);
        routes = new HashMap<>();
        routes.put("/counts", getCounts());
        routes.put("/nb-projects-by-company", getNbProjectsByCompany());
        routes.put("/projects", getFullProjects());
    }


    private Consumer<PrintWriter> getCounts() {
        return writer -> {
            writer.println("Companies:" + companyService.countAll());
            writer.println("Customers:" + customerService.countAll());
            writer.println("BusinessTypes:" + businessTypeService.countAll());
            writer.println("Projects:" + projectService.countAll());
        };
    }


    private Consumer<PrintWriter> getNbProjectsByCompany() {
        return writer -> {
            Map<String, Integer> all = companyService.getAllWithProjectCount();
            all.forEach((k, v) -> writer.println(k + ":" + v));
        };
    }

    private Consumer<PrintWriter> getFullProjects() {
        return writer -> {
            writer.println("[");
            List<String> content = new ArrayList<>();
            for (Project project : projectService.findAll()) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                sb.append("\"name\":\"").append(project.getName()).append("\",");
                sb.append("\"customer\":\"").append(project.getCustomer().getName()).append("\",");
                sb.append("\"company\":\"").append(project.getCompany().getName()).append("\"");
                sb.append("}");
                content.add(sb.toString());
            }
            writer.println(String.join(",", content));
            writer.println("]");
        };
    }


    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final PrintWriter writer = resp.getWriter();
        routes.get(req.getPathInfo()).accept(writer);
    }
}
