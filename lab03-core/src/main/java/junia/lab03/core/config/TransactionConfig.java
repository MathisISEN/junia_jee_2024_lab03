package junia.lab03.core.config;

import jakarta.persistence.EntityManagerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement()
@ImportResource("classpath:META-INF/transaction-context.xml")
public class TransactionConfig {

    @Bean
    public JpaTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory, DataSource dataSource) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory);
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }
}
