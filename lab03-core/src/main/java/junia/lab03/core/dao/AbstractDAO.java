package junia.lab03.core.dao;

import junia.lab03.core.entity.GenericEntity;
import junia.lab03.core.repository.GenericRepository;
import junia.lab03.domain.dao.GenericDAO;
import org.dozer.DozerBeanMapper;

import java.util.List;
import java.util.stream.Collectors;

//TODO create 4 DAOs which inherits this abstract class
public abstract class AbstractDAO<T, U extends GenericEntity> implements GenericDAO<T> {

    private final GenericRepository<U> genericRepository;

    private final DozerBeanMapper mapper;

    private final T dataObject;
    private final U entityObject;

    protected AbstractDAO(GenericRepository<U> genericRepository, T dataObject, U entityObject) {
        this.genericRepository = genericRepository;
        this.mapper = new DozerBeanMapper();
        this.dataObject = dataObject;
        this.entityObject = entityObject;
    }


    @Override
    public void deleteAll() {
        this.genericRepository.deleteAll();
    }

    @Override
    public void save(T objectToSave) {
        Class<U> uclass = (Class<U>) entityObject.getClass();
        this.genericRepository.save(mapper.map(objectToSave, uclass));

    }

    @Override
    public long count() {
        return this.genericRepository.count();
    }

    @Override
    public List<T> findAll() {
        Class<T> tclass = (Class<T>) dataObject.getClass();
        return this.genericRepository.findAll().stream().map(u -> mapper.map(u, tclass)).collect(Collectors.toList());
    }

    public GenericRepository<U> getGenericRepository() {
        return genericRepository;
    }
}
